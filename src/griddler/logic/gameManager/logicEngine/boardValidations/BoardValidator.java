package griddler.logic.gameManager.logicEngine.boardValidations;

import griddler.logic.descriptor.GameDescriptor;
import griddler.logic.descriptor.Slice;
import griddler.logic.descriptor.Square;
import griddler.logic.gameManager.logicEngine.exceptions.GriddlerLogicException;
import griddler.logic.gameManager.logicEngine.exceptions.GriddlerValidBoardException;

import java.awt.*;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Omri on 13/8/2016.
 * This class performs validation tests on new XML files we wish to load
 */
public class BoardValidator {
    public static final String rowDescriptor = "row";
    private static final String columnDescriptor = "column";
    private static final int maxColumnSize = 100;
    private static final int minColumnSize = 10;
    private static final int maxRowSize = 100;
    private static final int minRowSize = 10;

    public Boolean isValidGameDescriptor(GameDescriptor gameDescriptor) throws GriddlerLogicException {
        Boolean isValid;
        /*
        we check rules 3-6 in the exercise instructions
         */
        try {
            isValid = runValidTests(gameDescriptor);
        } catch (GriddlerLogicException e) {
            throw new GriddlerValidBoardException(e.getMessage());
        }

        return isValid;
    }
    /*
    Aggregation of the different tests
     */
    private boolean runValidTests(GameDescriptor gameDescriptor)throws  GriddlerLogicException {
        return isValidNumberOfRowsAndCols(gameDescriptor) && isValidSlices(gameDescriptor)
                && isValidSolution(gameDescriptor) && isSliceForEachRowAndCol(gameDescriptor) &&isBoardDimensionValid(gameDescriptor);
    }

    /*
    We create a hash table for each collection and see if each row/ col has a slice assigned to it
     */
    @SuppressWarnings("SuspiciousMethodCalls")
    private boolean isSliceForEachRowAndCol(GameDescriptor gameDescriptor) throws GriddlerValidBoardException {
        HashMap<Integer, Integer> hashMapCols = new HashMap<>();
        HashMap<Integer, Integer> hashMapRows = new HashMap<>();
        HashMap<Integer, Integer> currentHashMap;
        int numOfRows, numOfCols;
        boolean isValid = true;
        numOfCols = gameDescriptor.getBoard().getDefinition().getColumns().intValue();
        numOfRows = gameDescriptor.getBoard().getDefinition().getRows().intValue();

        for (int i = 0; i < numOfCols + numOfRows; i++) {
            Slice slice = gameDescriptor.getBoard().getDefinition().getSlices().getSlice().get(i);

            if (slice.getOrientation().equals(rowDescriptor)) {
                currentHashMap = hashMapRows;
            } else {
                currentHashMap = hashMapCols;
            }

            if (currentHashMap.containsKey(slice.getId())) {
                isValid = false;
                throw new GriddlerValidBoardException(slice.getOrientation() + " " + slice.getId() + " is defined more than once!");
            } else {
                currentHashMap.put(slice.getId().intValue(), slice.getId().intValue());
            }
        }
        return isValid;
    }

    /*
    1 .We create a hashtable for all our solution blocks, and insert,
    if we find a duplicate - invalid solution
    2. We check that the positions are in the game boundaries
     */
    private boolean isValidSolution(GameDescriptor gameDescriptor) throws GriddlerValidBoardException {
        boolean isValid = true;
        int numOfRows, numOfCols;
        HashMap<Point, Point> hashMap = new HashMap<>();
        List<Square> solution = gameDescriptor.getBoard().getSolution().getSquare();

        numOfCols = gameDescriptor.getBoard().getDefinition().getColumns().intValue();
        numOfRows = gameDescriptor.getBoard().getDefinition().getRows().intValue();

        for (Square square : solution) {
        /*
        Check that the cell in in bounds
         */
            if (square.getColumn().intValue() > numOfCols || square.getColumn().intValue() < 1 ||
                    square.getRow().intValue() > numOfRows || square.getRow().intValue() < 1) {
                throw new GriddlerValidBoardException("Invalid square in solution, " + square.getColumn() + square.getRow());
            } else {
                /*
                if it's in bound check for duplicates
                 */
                Point point = new Point(square.getColumn().intValue(), square.getRow().intValue());
                boolean he = hashMap.containsKey(point);

                if (!hashMap.containsKey(point)) {
                    hashMap.put(point, point);
                } else {
                    isValid = false;
                    throw new GriddlerValidBoardException("Duplicate cells in solution" + point.toString());
                }
            }
        }
        return isValid;
    }

    /*
    We check that none of the slices hold more blocks than the row/column
    Tests number 4 in exercise
     */
    private boolean isValidSlices(GameDescriptor gameDescriptor) throws GriddlerValidBoardException {
        return false;
    }

    /*
    We get the number of rows and cols, and check that each has the correct number of slices.
    Tests number 3 in exercise
     */
    private boolean isValidNumberOfRowsAndCols(GameDescriptor gameDescriptor) throws GriddlerValidBoardException {
        Boolean isValid;
        int numOfRows, numOfCols;

        numOfCols = gameDescriptor.getBoard().getDefinition().getColumns().intValue();
        numOfRows = gameDescriptor.getBoard().getDefinition().getRows().intValue();

        for (Slice slice : gameDescriptor.getBoard().getDefinition().getSlices().getSlice()) {
            if (slice.getOrientation().equals(rowDescriptor)) {
                numOfRows--;
            }
            if (slice.getOrientation().equals(columnDescriptor)) {
                numOfCols--;
            }
        }
        if (numOfCols == 0 && numOfRows == 0) {
            isValid = true;
        } else {
            throw new GriddlerValidBoardException("Invalid number of rows and cols to slices!");
        }

        return isValid;
    }

    /*
    Check the dimensions of the board in the discriptor
     */
    private boolean isBoardDimensionValid(GameDescriptor gameDescriptor) throws GriddlerValidBoardException {
        Boolean isValid = true;
        int numOfRows, numOfCols;

        numOfCols = gameDescriptor.getBoard().getDefinition().getColumns().intValue();
        numOfRows = gameDescriptor.getBoard().getDefinition().getRows().intValue();

        if (numOfCols < minColumnSize) {
            isValid = false;
        } else {
            throw new GriddlerValidBoardException("Too few columns in game file!");
        }
        if (numOfCols > maxColumnSize) {
            isValid = false;
        } else {
            throw new GriddlerValidBoardException("Too many columns in game file!");
        }

        if (numOfRows < minRowSize) {
            isValid = false;
        } else {
            throw new GriddlerValidBoardException("Too few rows in game file!");
        }

        if (numOfRows > maxRowSize) {
            isValid = false;
        } else {
            throw new GriddlerValidBoardException("Too many rows in game file!");
        }
        return isValid;
    }
}
