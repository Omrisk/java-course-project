package griddler.logic.gameManager.logicEngine.exceptions;

/**
 * Created by Omri on 12/8/2016.
 */
public class GriddlerLogicException extends Exception {
    String message;

    GriddlerLogicException() {
        this.message = "New exception!";
    }

    public GriddlerLogicException(String s) {
        this.message = s;
    }


    @Override
    public String getMessage() {
        return message;
    }
}

