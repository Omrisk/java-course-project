package griddler.logic.gameManager.logicEngine.exceptions;

/**
 * Created by Omri on 12/8/2016.
 */
public class GriddlerGameNotFoundException extends GriddlerLogicException {


    public GriddlerGameNotFoundException() {
        message = "Game file not found!";
    }

}
