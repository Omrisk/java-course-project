package griddler.logic.gameManager.gameRoom;

import griddler.logic.gameManager.gameRoom.board.Board;
import griddler.logic.gameManager.gameRoom.board.Cell;
import griddler.logic.gameManager.gameRoom.board.CellStatus;
import griddler.logic.gameManager.gameRoom.board.Solution;

import java.awt.*;
import java.sql.Time;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;


/**
 * Created by Omri on 12/8/2016.
 * Class for keeping track of score, and sparingly accessed information
 */
public class Stats {
    /*
    Hold references to The players board and the game room's solution to calculate scores!
     */
    private Cell[][] playerBoard;
    private Solution solution;
    private long gameStartTime;
    private int score, numberOfUndos, numOfMovesPerformed;

    public Stats() {
        numberOfUndos = numOfMovesPerformed = 0;
    }

    public Stats(Solution _solution) {
        super();
        solution = _solution;
    }
    /*
    Creates our time stamp
     */
    public void init() {
        initTime();
        numberOfUndos = numOfMovesPerformed = 0;
    }

    public String getGameStartTime() {
        long currentTime = new Date().getTime();

        long diff = currentTime - gameStartTime;
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000);

        return String.format("Elapsed time since game start: %s Hours, %s minutes and %s seconds", diffHours, diffMinutes, diffSeconds);
    }

    /*
    we get the solution points as a stream, and count the matches
    we decide to get a score between 0-100
     */
    public int getScore() {
        double matchCount = 0;

        for (Point point : solution.getSolutionMap().keySet()) {
            if (playerBoard[(int) point.getY()][(int) point.getX()].getCellStatus().equals(CellStatus.BLACK)) {
                matchCount++;
            }
        }
        /*
        convert to a score between 0-100
         */
        double tempScore = matchCount / (getSolutionCellCount());
        score = (int) (tempScore * 100);
        // Prevent score overflowing
        if (score > 100) {
            score = 100;
        }
        return score;
    }

    public void initTime() {
        gameStartTime = new Date().getTime();
    }

    public void setSolution(Solution _solution) {
        solution = _solution;
    }

    public void setPlayerBoard(Cell[][] board) {
        playerBoard = board;
    }

    public void incNumOfMovePerformed() {
        numOfMovesPerformed++;
    }

    public void incNumOfUndoesPerformed() {
        numberOfUndos++;
    }

    public int getMovesPerformedCount() {
        return numOfMovesPerformed;
    }

    public int getUndoesPerformedCount() {
        return numberOfUndos;
    }

    public int getSolutionCellCount() {
        return solution.getNumOfCellsInSolution();
    }
}
