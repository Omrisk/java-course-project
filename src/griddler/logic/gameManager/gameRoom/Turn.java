package griddler.logic.gameManager.gameRoom;

import griddler.logic.gameManager.gameRoom.board.CellStatus;
import griddler.logic.gameManager.logicEngine.boardValidations.BoardValidator;
import griddler.logic.gameManager.logicEngine.exceptions.GriddlerLogicException;
import org.jetbrains.annotations.Contract;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Omri on 12/8/2016.
 * A turn is a list of moves
 */
public class Turn {
    private List<Move> moves;

    Turn() {
        moves = new LinkedList<>();
    }

    public Turn(List<Move> movesList) {
        moves = movesList;
    }

    public List<Move> getMoves() {
        return moves;
    }

    @Contract("_ -> !null")
    public static Turn parse(String string) throws GriddlerLogicException {

        Turn turn;
        if (Pattern.matches("[r|c](>>)([0-9]|[0-9][0-9])(>>)([0-9]|[0-9][0-9])(>>)([0-9]|[0-9][0-9])(>>)[b|w|u]$", string)) {
            turn = getTurnFromString(string);
        } else {
            throw new GriddlerLogicException("Invalid turn format!");
        }

        return turn;
    }

    /*
    1. We split our string into separate pieces so that we can index them
    2. We check orientation so we can default the indexing of the new Moves
    3. We add each separate new move to the moves list
     */
    private static Turn getTurnFromString(String string) throws GriddlerLogicException {
        Turn turnToReturn = new Turn();
        String[] turnAsAString = string.split(">>");
        final String rowSymbolForParse = "r";
        int startIndex = Integer.parseInt(turnAsAString[1]);
        int startOfRange = Integer.parseInt(turnAsAString[2]);
        int endOfRange = Integer.parseInt(turnAsAString[3]);
        int range = endOfRange - startOfRange;

        if (startIndex > endOfRange) {
            throw new GriddlerLogicException("End of range is smaller the beginning of range!");
        }
        /*
        Adding thew moves!
         */
        if (turnAsAString[0].equals(rowSymbolForParse)) {
            for (int i = 0; i <= range; i++) {
                turnToReturn.moves.add(new Move(startOfRange++, startIndex, CellStatus.Parse(turnAsAString[4])));
            }
        } else {
            for (int i = 0; i <= range; i++) {
                turnToReturn.moves.add(new Move(startIndex, startOfRange++, CellStatus.Parse(turnAsAString[4])));
            }
        }

        return turnToReturn;
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        for (Move move : moves) {
            string.append(moves.toString());
        }

        return string.toString();
    }
}

