package griddler.logic.gameManager.gameRoom;

import griddler.logic.descriptor.*;
import griddler.logic.gameManager.gameRoom.board.*;
import griddler.logic.gameManager.gameRoom.board.Slice;
import griddler.logic.gameManager.gameRoom.board.Slices;
import griddler.logic.gameManager.logicEngine.LogicEngine;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Omri on 12/8/2016.
 * Manages a single game room
 * Is built to hold: A single game board + dynamic number of players
 */
public class GameRoom {
    private List<Player> playersList;
    private Player currentPlayer;
    private Board board;
    private LogicEngine gameLogic;
    /*Marks an initialized game*/
    private boolean gameInProgress = false;
    private int moves;

    public int getMoves() {
        return moves;
    }

    /*
        We assume that the game descriptor is valid (has been checked by our logic module)
         */
    public GameRoom(GameDescriptor gameDescriptor) {
        board = new Board(gameDescriptor.getBoard());

        playersList = this.getPlayersListFromGameDescriptor(gameDescriptor.getMultiPlayers());
        moves = Integer.parseInt(gameDescriptor.getMultiPlayers().getMoves());
        currentPlayer = playersList.get(0);
    }

    private List<Player> getPlayersListFromGameDescriptor(GameDescriptor.MultiPlayers multiPlayers) {
        int mutliPlayersSize = multiPlayers.getPlayers().getPlayer().size();
        List<Player> playerList = new ArrayList<>(mutliPlayersSize);
        /*
        create the player list with each "node" a human or computer player
         */
        for (int i = 0; i < mutliPlayersSize; i++) {
            if (multiPlayers.getPlayers()
                    .getPlayer()
                    .get(i)
                    .getPlayerType()
                    .equalsIgnoreCase("Human")) {
                playerList.add(new HumanPlayer(multiPlayers.getPlayers().getPlayer().get(i), board.getNumOfCols(), board.getNumOfRows()));
            } else if (
                    multiPlayers.getPlayers()
                            .getPlayer()
                            .get(i)
                            .getPlayerType()
                            .equalsIgnoreCase("Computer")) {
                playerList.add(new AIPlayer(multiPlayers.getPlayers().getPlayer().get(i), board.getNumOfCols(), board.getNumOfRows(), Integer.parseInt(multiPlayers.getMoves())));
            }
        }

        return playerList;
    }

    private void setGameInProgress(boolean gameInProgress) {
        this.gameInProgress = gameInProgress;
    }

    public boolean isGameInProgress() {
        return gameInProgress;
    }

    public void initGame() {

        //Init players!
        getPlayersList().forEach(player -> player.init(board.getSolution()));
        //Init Slices with block index
        for (Slice s : board.getColSlices().getSliceList()) {
            s.UpdateBlocksInSlice(board.getNumOfCols());
        }
        for (Slice s : board.getRowSlices().getSliceList()) {
            s.UpdateBlocksInSlice(board.getNumOfRows());
        }
        setGameInProgress(true);
    }


    public boolean validateTurn(Turn turnToPerform) {
        /*
        We check each move in the turn for validity
        if all are valid -> the move is OK
         */
        return turnToPerform.getMoves().stream().
                allMatch(move -> move.getRow() < getBoardRows() && move.getCol() < getBoardCols());
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    /*
    Finds the player position and advances the the next one or sends us back to the beginning of the list
     */
    public void advanceToNextPlayer() {
        Iterator<Player> iterator = playersList.iterator();
        Player player;

        while (iterator.hasNext()) {
            player = iterator.next();
            if (player.equals(currentPlayer)) {
                if (iterator.hasNext()) {
                    currentPlayer = iterator.next();
                } else {
                    currentPlayer = playersList.get(0);
                }
            }
        }
    }

    public int getBoardCols() {
        return board.getNumOfCols();
    }

    public int getBoardRows() {
        return board.getNumOfRows();
    }

    public List<Player> getPlayersList() {
        return playersList;
    }

    public Slices getRowSlices() {
        return board.getRowSlices();
    }

    public Slices getColSlices() {
        return board.getColSlices();
    }
}
