package griddler.logic.gameManager.gameRoom;

import griddler.logic.gameManager.gameRoom.board.Cell;
import griddler.logic.gameManager.gameRoom.board.CellStatus;
import griddler.logic.gameManager.gameRoom.board.Solution;
import sun.awt.image.ImageWatched;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Omri on 12/8/2016.
 * Our Abstracted player class, aggregates a instance of a player
 */
public class Player {
    private String name;
    private Cell[][] playerBoard;
    private LinkedList<Turn> turnList;
    private LinkedList<Turn> redoList;
    PlayerType playerType;
    private Stats playerStats;
    private int id;


    public enum PlayerType {
        HUMAN, AI
    }

    Player() {
        turnList = new LinkedList<>();
        redoList = new LinkedList<>();
        playerStats = new Stats();
    }

    Player(griddler.logic.descriptor.Player player, int cols, int rows) {
        this();
        name = player.getName();
        turnList = new LinkedList<>();
        this.initializePlayerBoard(cols, rows);
        id = player.getId().intValue();
    }

    public Cell[][] getPlayerBoard() {
        return playerBoard;
    }

    public boolean isComputerPlayer() {
        return playerType.equals(PlayerType.AI);
    }

    public String getIDAsString() {
        return String.valueOf(id);
    }


    public Cell getBoardValueAtXY(int x, int y) {
        return playerBoard[y][x];
    }

    public PlayerType getPlayerType() {
        return playerType;
    }

    public LinkedList<Turn> getTurnList() {
        return turnList;
    }

    public String getName() {
        return name;
    }

    public Stats getStats() {
        return playerStats;
    }

    /*
    Inits the player starting values and stats module (allows checking the solution for:score and blocks
     */
    void init(Solution solution) {
        playerStats.init();
        playerStats.setSolution(solution);
        playerStats.setPlayerBoard(playerBoard);
    }

    /*
    even if the score is 100 the player may have just painted the board black
    so we check
     */
    public boolean isGameWon() {
        boolean hasPlayerWon = false;

        if (playerStats.getScore() == 100) {
            int solutionCount = playerStats.getSolutionCellCount(), blackCellCount = 0;

            for (Cell[] cellRow : playerBoard) {
                for (Cell currentCell : cellRow) {
                    if (currentCell.getCellStatus().equals(CellStatus.BLACK)) {
                        blackCellCount++;
                    }
                }
            }
            if (blackCellCount == solutionCount) {
                hasPlayerWon = true;
                setUndefinedToWhite();
            }
        }
        return hasPlayerWon;
    }

    /*
     If a game has ended we may want to color all cells as "white"
     */
    public void setUndefinedToWhite() {
        for (Cell[] cellRow : playerBoard) {
            for (Cell currentCell : cellRow) {
                if (currentCell.getCellStatus().equals(CellStatus.UNDEFINED)) {
                    currentCell.updateCellStatus(CellStatus.WHITE);
                }
            }
        }
    }

    /*
    called when performing a new turn for a player
    */
    public void performTurn(Turn newTurn) {
        CellStatus tempCellStatus;
        Cell tempCell;

        /*
        we get each cell and then:
        1. Save the current status of the cell to the old move status
        2. Set the cell's new status to be the new move's status
        3. Update the oldCellStatus to be the old status of the cell
         */
        for (Move move : newTurn.getMoves()) {
            tempCell = playerBoard[move.getRow()][move.getCol()];
            move.setOldCellStatus(tempCell.getCellStatus());
            tempCell.updateCellStatus(move.getCurrentCellStatus());
        }
        /*
        Add the move we just performed to the list of turns!
         */
        turnList.add(newTurn);
        playerStats.incNumOfMovePerformed();
    }

    /*
    Undoing entails the following:
    1. Get the last turn
    2. Since each move holds the cell's old status we change the cell back to that
    3. We save the last move as the redo turn if needed
    4. remove the last turn from the list
    5. inc the undo counter
     */
    public void performUndo() {
        Cell tempCell;
        if (!turnList.isEmpty()) {
            //1.
            Turn lastTurnMade = turnList.removeLast();
            // 3.
            redoList.addFirst(lastTurnMade);
            // 2.
            for (Move move : lastTurnMade.getMoves()) {
                tempCell = playerBoard[move.getRow()][move.getCol()];
                tempCell.updateCellStatus(move.getOldCellStatus());
            }
            //4.
            playerStats.incNumOfUndoesPerformed();
        }
    }

    private void initializePlayerBoard(int cols, int rows) {
        playerBoard = new Cell[rows][];

        /*
        initialize the array
         */
        for (int i = 0; i < rows; i++) {
            playerBoard[i] = new Cell[cols];
        }
        /*
        and fill it
         */
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                playerBoard[i][j] = new Cell(i, j);
            }
        }
    }

    /*
    We get the redo turn
    and feed it to the player to perform again
     */
    public void performRedo() {
        if (!redoList.isEmpty()) {
            Turn redoTurn = redoList.removeFirst();
            turnList.addLast(redoTurn);
            performTurn(redoTurn);
        }
    }

    public int getIdAsInt() {
        return id;
    }

    /*
    For AIPlayer override until exercise 2
    */
    public Boolean canPlay() {
        return true;
    }

    public boolean isTurnListEmpty() {
        return turnList.isEmpty();
    }

    public boolean isRedoListEmpty() {
        return redoList.isEmpty();
    }

    public LinkedList<Turn> getRedoList() {
        return redoList;
    }
}