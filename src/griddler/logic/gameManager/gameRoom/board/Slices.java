package griddler.logic.gameManager.gameRoom.board;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Omri on 12/8/2016.
 * Represents a slice on the griddler board
 */
public class Slices {
    private List<Slice> slices;
    private Orientation sliceOrientation;

    public enum Orientation {
        Rows,
        Columns
    }

    Slices(int size, Orientation orientation) {
        slices = new ArrayList<>(size);
        this.sliceOrientation = orientation;

        for (int i = 0; i < size; i++) {
            slices.add(i, new Slice());
        }
    }

    /*
    adds a new "slice to the array
     */
    public Slice getSliceInList(int index) {
        return slices.get(index);
    }

    public List<Slice> getSliceList() {
        return slices;
    }

}
