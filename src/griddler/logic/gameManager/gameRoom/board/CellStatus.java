package griddler.logic.gameManager.gameRoom.board;

/**
 * Created by Omri on 12/8/2016.
 * Enum class for the diffrent stats a cell can be
 */
public enum CellStatus {
    UNDEFINED(0), WHITE(1), BLACK(2);

    private final int selection;

    CellStatus(int _selection) {
        selection = _selection;
    }

    public static CellStatus Parse(String s) {
        CellStatus cellStatus;
        switch (s) {
            case ("w"):
                cellStatus = CellStatus.WHITE;
                break;
            case ("b"):
                cellStatus = CellStatus.BLACK;
                break;
            case ("u"):
            default:
                cellStatus = CellStatus.UNDEFINED;
                break;

        }
        return cellStatus;
    }
}