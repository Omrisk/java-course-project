package griddler.logic.gameManager.gameRoom.board;

import java.awt.*;

/**
 * Created by Omri on 12/8/2016.
 * Our Board will comprise of individual cells that know their own position and currect status
 */
public class Cell {
    private int row, col;
    private CellStatus cellStatus;

    private int getCol() {
        return col;
    }

    private int getRow() {
        return row;
    }

    public CellStatus getCellStatus() {
        return cellStatus;
    }

    public Cell(int _row, int _col) {
        row = _row;
        col = _col;
        cellStatus = CellStatus.UNDEFINED;
    }

    private Cell(int _row, int _col, CellStatus _status) {
        this(_row, _col);
        cellStatus = _status;
    }

    public void updateCellStatus(CellStatus newStatus) {
        cellStatus = newStatus;
    }

    public Point CellToPoint() {
        return new Point(this.getCol(), this.getRow());
    }

    /*
    For printing the cells
     */
    @Override
    public String toString() {
        switch (cellStatus) {
            case BLACK:
                return "X";
            case UNDEFINED:
                return "-";
            case WHITE:
                return "+";
            default:
                return " ";
        }
    }
}
