package griddler.logic.gameManager.gameRoom.board;

/**
 * A block in a slice
 */
public class Block {
    Integer block;
    boolean isPerfect;
    private int minRange;
    private int maxRange;
    private int startCell;


    public Block(Integer block) {
        this.block = block;
        isPerfect = false;
    }

    public Integer getBlock() {
        return block;
    }

    public void setBlock(Integer block) {
        this.block = block;
    }

    public int getMinRange() {
        return minRange;
    }

    void setMinRange(int minRange) {
        this.minRange = minRange;
    }

    public int getMaxRange() {
        return maxRange;
    }

    void setMaxRange(int maxRange) {
        this.maxRange = maxRange;
    }

    public void setStartCell(int startCell) {
        this.startCell = startCell;
    }

    public boolean isPerfect() {
        return isPerfect;
    }

    public void setPerfect(boolean perfect) {
        isPerfect = perfect;
    }
}
