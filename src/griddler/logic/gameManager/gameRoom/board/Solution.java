package griddler.logic.gameManager.gameRoom.board;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Created by Omri on 12/8/2016.
 * Our solution is represented as a HashMap to allow easy access and checking of solutions
 */
public class Solution {
    private HashMap<Point, Point> solution;
    private int numOfCellsInSolution;

    public int getNumOfCellsInSolution() {
        return numOfCellsInSolution;
    }

    private Solution() {
        solution = new HashMap<>();
    }

    Solution(griddler.logic.descriptor.Solution _solution) {
        int x, y;
        numOfCellsInSolution = _solution.getSquare().size();
        solution = new HashMap<>(numOfCellsInSolution);
        /*
        Get value of square position and set cell to it as well
         */
        for (int i = 0; i < numOfCellsInSolution; i++) {
            x = _solution.getSquare().get(i).getColumn().intValue();
            y = _solution.getSquare().get(i).getRow().intValue();
            Point point = new Point(x - 1, y - 1);
            solution.put(point, point);
        }
    }

    public HashMap<Point, Point> getSolutionMap() {
        return solution;
    }

    Stream<Map.Entry<Point, Point>> getSolutionAsStream(){
        return solution.entrySet().stream();
    }
}
