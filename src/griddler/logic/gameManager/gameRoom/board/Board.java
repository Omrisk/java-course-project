package griddler.logic.gameManager.gameRoom.board;

import griddler.logic.descriptor.GameDescriptor;
import griddler.logic.descriptor.Slice;
import griddler.logic.gameManager.logicEngine.boardValidations.BoardValidator;

import java.util.List;

/**
 * Created by Omri on 12/8/2016.
 * A game board, consists of the solution and slice definition for the rows and cols
 */
public class Board {
    private Solution solution;
    private Slices rowSlices;
    private Slices colSlices;
    private int numOfRows, numOfCols;

    public Board() {
    }

    public int getNumOfRows() {
        return numOfRows;
    }

    public int getNumOfCols() {
        return numOfCols;
    }

    public Board(GameDescriptor.Board _board) {
        numOfCols = _board.getDefinition().getColumns().intValue();
        numOfRows = _board.getDefinition().getRows().intValue();

        solution = new Solution(_board.getSolution());
        this.initializeSlices(_board.getDefinition().getSlices().getSlice());

    }

    public Solution getSolution() {
        return solution;
    }

    /*
    Assume size of Board has already been initialized
     */
    private void initializeSlices(List<Slice> slice) {
        int numOfRows = this.numOfRows, numOfCols = this.numOfCols, currentSliceID;
        rowSlices = new Slices(numOfRows, Slices.Orientation.Rows);
        colSlices = new Slices(numOfCols, Slices.Orientation.Columns);
        String blocks;
        String[] splicedBlocks;

       /*
        We add row/column blocks to the appropriate list
       */
        for (int i = 0; i < numOfCols + numOfRows; i++) {
            /*
            Get the blocks for the slice
             */
            blocks = slice.get(i).getBlocks();
            blocks = blocks.replaceAll("\\s+", "");
            splicedBlocks = blocks.split(",");
            currentSliceID = slice.get(i).getId().intValue();
            if (slice.get(i).getOrientation().equals(BoardValidator.rowDescriptor)) {
                rowSlices.getSliceInList(currentSliceID - 1).setId(currentSliceID - 1);
            }else{
                colSlices.getSliceInList(currentSliceID - 1).setId(currentSliceID - 1);
            }
            for (String block : splicedBlocks) {
                if (slice.get(i).getOrientation().equals(BoardValidator.rowDescriptor)) {
                    rowSlices.getSliceInList(currentSliceID - 1).add(Integer.parseInt(block));
                } else {
                    colSlices.getSliceInList(currentSliceID - 1).add(Integer.parseInt(block));
                }
            }
        }
    }

    public Slices getRowSlices() {
        return rowSlices;
    }

    public Slices getColSlices() {
        return colSlices;
    }
}
