package griddler.logic.gameManager.gameRoom;

import griddler.logic.gameManager.gameRoom.board.*;

/**
 * Created by Omri on 12/8/2016.
 * A move is a position and the new CellStatus that the Position needs to hold
 */
public class Move {
    private int col, row;
    private CellStatus currentCellStatus, oldCellStatus;

    public Move(int _col, int _row, CellStatus _cellStatus) {
        col = _col;
        row = _row;
        currentCellStatus = _cellStatus;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public int getRow() {

        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    CellStatus getCurrentCellStatus() {
        return currentCellStatus;
    }

    public void setCurrentCellStatus(CellStatus _currentCellStatus) {

        oldCellStatus = currentCellStatus;
        currentCellStatus = _currentCellStatus;
    }

    void setOldCellStatus(CellStatus _cellStatus) {
        oldCellStatus = _cellStatus;
    }

    CellStatus getOldCellStatus() {
        return oldCellStatus;
    }

    @Override
    public String toString() {
        String string;

        string = String.format("Marked cell {%s,%s} as %s ", this.getCol(), getRow(), getCurrentCellStatus().name());

        return string;
    }
}
