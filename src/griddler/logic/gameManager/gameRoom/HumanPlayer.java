package griddler.logic.gameManager.gameRoom;


/**
 * Created by Omri on 12/8/2016.
 * Simplfied Human player
 */
class HumanPlayer extends Player {
    private HumanPlayer() {
    }

    HumanPlayer(griddler.logic.descriptor.Player player, int cols, int rows) {
        super(player, cols, rows);

        playerType = PlayerType.HUMAN;
    }
}
