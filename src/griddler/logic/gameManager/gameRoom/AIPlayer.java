package griddler.logic.gameManager.gameRoom;

/**
 * Created by Omri on 12/8/2016.
 * Extends the player class for AI players
 */
class AIPlayer extends Player {
    private int remainingTurns;

    AIPlayer(griddler.logic.descriptor.Player player, int numOfCols, int numOfRows, int numOfTurns) {
        super(player, numOfCols, numOfRows);
        remainingTurns = numOfTurns;
        playerType = PlayerType.AI;
    }
    /*
    Since only our AI has a move limitation currently, we perform the generic move and dec from the total allowed
     */
    @Override
    public void performTurn(Turn newTurn) {
        super.performTurn(newTurn);
        remainingTurns--;
    }

    @Override
    public Boolean canPlay() {
        return remainingTurns > 0;
    }
}
