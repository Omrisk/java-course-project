package griddler.logic.gameManager;

import griddler.logic.gameManager.gameRoom.GameRoom;
import griddler.logic.descriptor.*;
import griddler.logic.gameManager.logicEngine.LogicEngine;
import griddler.logic.gameManager.logicEngine.exceptions.*;
import static griddler.logic.gameLoader.gameMonitorCreator.CreateGameDescriptor;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by Omri on 12/8/2016.
 * Manages our collection of ongoing games
 */
public class GameManager {
    private List<GameRoom> gameRoomList;
    private LogicEngine logicEngine;

    public List<GameRoom> getGameRoomList() {
        return gameRoomList;
    }

    public GameManager() {
        logicEngine = new LogicEngine();
        gameRoomList = new LinkedList<GameRoom>();
    }

    public LogicEngine getLogicEngine() {
        return logicEngine;
    }

    public void LoadNewGame(String gameFilePath) throws GriddlerLogicException, java.io.FileNotFoundException, JAXBException {
        GameDescriptor gameDescriptor;

        /*
        Attempt to load a XML game file, and forwards throw exceptions to calling funct
        for handling
         1. Create the GameDescriptor (requires valid XML file)
         2. Validate that the game is valid (According to the game exercise instructions
         */
        gameDescriptor = CreateGameDescriptor(gameFilePath);
       /*
       We check if the game is valid (exceptions are thrown if not to the user)
       if isValid we create a new game room
        */
        if(logicEngine.validateGameDescriptor(gameDescriptor)) {
            gameRoomList.add(new GameRoom(gameDescriptor));
        }

    }
    /*
    When initializing a game we take the default game (Since we currently only have 1 concurrent game)
    Set it to be a game in progress and Mark the players starting time
     */
    public void InitializeGame() {
        GameRoom defaultGameRoom = gameRoomList.get(0);
        defaultGameRoom.initGame();
    }

    public  boolean isGameInProgress(){
        return gameRoomList.get(0).isGameInProgress();
    }
}
