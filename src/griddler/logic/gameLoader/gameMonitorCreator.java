package griddler.logic.gameLoader;

import griddler.logic.descriptor.GameDescriptor;
import griddler.logic.gameManager.logicEngine.exceptions.GriddlerGameNotFoundException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class gameMonitorCreator {
    final static String xmlSuffix = ".xml";

    /*
    java.io.FileNotFoundException is throw if the file is missing
    javax.xml.bind.UnmarshalException is thrown if the xml is invalid
     */
    public static GameDescriptor CreateGameDescriptor(String path) throws java.io.FileNotFoundException, javax.xml.bind.UnmarshalException, GriddlerGameNotFoundException {
        /* We receive a full path to a XML file describing the new game*/
        GameDescriptor game;
        try {

            File file = new File(path);
            JAXBContext jaxbContext = JAXBContext.newInstance(GameDescriptor.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            game = (GameDescriptor) jaxbUnmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            game = null;
            throw new GriddlerGameNotFoundException();
        }

        return game;
    }
}
