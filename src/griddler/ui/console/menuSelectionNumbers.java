package griddler.ui.console;

/**
 * Created by Omri on 12/8/2016.
 * To help us with selecting our menu items,
 * We enumerate each one of our items!
 */
public enum menuSelectionNumbers {
    INVALID_SELECTION(0),
    LOAD_A_NEW_GAME(1),
    BEGIN_GAME(2),
    SHOW_CURRENT_GAME_STATUS(3),
    PERFORM_MOVE(4),
    PRINT_MOVE_LIST(5),
    UNDO(6),
    STATS(7),
    END_GAME(8),
    REDO(9);

    private final int selection;

    /*
    For Converting from an in to ENUM, if no match is found return a INVALID_SELECTION
    */
    public static menuSelectionNumbers getEnumFronInt(int intToConvert) {
        menuSelectionNumbers result = INVALID_SELECTION;

        for(menuSelectionNumbers currentValue : menuSelectionNumbers.values()){
            if(currentValue.getSelection() == intToConvert) {
                result = currentValue;
            }
        }

        return result;
    }

    menuSelectionNumbers(int _selection){
        selection = _selection;
    }

    int getSelection(){
        return this.selection;
    }
}
