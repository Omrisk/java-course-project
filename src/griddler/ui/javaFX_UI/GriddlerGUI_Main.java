package griddler.ui.javaFX_UI;

import griddler.ui.javaFX_UI.mainScene.GriddlerGUIController;
import griddler.ui.javaFX_UI.mainScene.GriddlerGUI_GameStaticParams;
import javafx.application.Application;
import javafx.scene.Scene;

import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class GriddlerGUI_Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane gameLayout = new BorderPane();

        primaryStage.setTitle("Griddler Game!");
        ScrollPane resizableGame = new ScrollPane(gameLayout);
        resizableGame.setFitToWidth(true);
        resizableGame.setFitToWidth(true);

        primaryStage.setScene(new Scene(resizableGame, Screen.getPrimary().getVisualBounds().getWidth() / 2, Screen.getPrimary().getVisualBounds().getHeight() / 2));

        primaryStage.getScene().getStylesheets().clear();
        primaryStage.getScene().getStylesheets().add(getClass().getResource(GriddlerGUI_GameStaticParams.getCssResource()).toExternalForm());

        new GriddlerGUIController(gameLayout, primaryStage.getScene());

        primaryStage.show();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}


