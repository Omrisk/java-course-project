package griddler.ui.javaFX_UI.controllers;

import griddler.ui.javaFX_UI.mainScene.GriddlerGUIController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.HBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class UI_BottomButtonsHBoxController extends HBox implements Initializable, CustomGriddlerController {
    @FXML
    private Button loadGriddlerGameButton;
    @FXML
    private Button replayMovesButton;
    @FXML
    private Button skinsButton;
    @FXML
    private ComboBox<String> skinSelectComboBox;

    public UI_BottomButtonsHBoxController() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("BottomButtonsHBox.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void setLoadGriddlerGameButton(Button loadGriddlerGameButton) {
        this.loadGriddlerGameButton = loadGriddlerGameButton;
    }

    @Override
    public void setControlsOnEvent(GriddlerGUIController griddlerGUIController) {
        loadGriddlerGameButton.setOnAction(griddlerGUIController);
        replayMovesButton.setOnAction(griddlerGUIController);
        skinSelectComboBox.setOnAction(griddlerGUIController);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        skinSelectComboBox.getItems().addAll("Skin A", "Skin B");
    }

    public void setIsDisableLoadGame(boolean b) {
        loadGriddlerGameButton.setDisable(b);
    }

    public ComboBox<String> getSkinSelectComboBox() {
        return skinSelectComboBox;
    }

    public Button getReplayMovesButton() {
        return replayMovesButton;
    }

    public void setReplayMovesButton(Button replayMovesButton) {
        this.replayMovesButton = replayMovesButton;
    }

    public Button getSkinsButton() {
        return skinsButton;
    }

    public void setSkinsButton(Button skinsButton) {
        this.skinsButton = skinsButton;
    }

    public Button getLoadGriddlerGameButton() {
        return loadGriddlerGameButton;
    }

    public Button getLoadGameButton() {
        return loadGriddlerGameButton;
    }

}
