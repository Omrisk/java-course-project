package griddler.ui.javaFX_UI.controllers;

import griddler.logic.gameManager.gameRoom.GameRoom;
import griddler.logic.gameManager.gameRoom.board.CellStatus;
import griddler.ui.javaFX_UI.mainScene.GriddlerGUIController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Omri on 13/9/2016.
 */
public class UI_RightVBoxController extends VBox implements Initializable, CustomGriddlerController {
    @FXML
    private VBox rightPanelVBox;
    @FXML
    private Label currentMoveLabel;
    @FXML
    private Label currentTurnLabel;
    @FXML
    private Label maxTurnsLabel;
    @FXML
    private Button submitMoveButton;
    @FXML
    private Button undoMoveButton;
    @FXML
    private Button endTurnButton;
    @FXML
    private Button endTheGameButton;
    @FXML
    private ToggleGroup colorToggleGroup;

    public UI_RightVBoxController() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("RightPaneVBox.fxml"));
        fxmlLoader.setController(this);
        fxmlLoader.setRoot(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void setControlsOnEvent(GriddlerGUIController griddlerGUIController) {
        submitMoveButton.setOnAction(griddlerGUIController);
        undoMoveButton.setOnAction(griddlerGUIController);
        endTurnButton.setOnAction(griddlerGUIController);
        endTheGameButton.setOnAction(griddlerGUIController);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void initRightVBoxController(GameRoom gameRoom) {
        rightPanelVBox.setDisable(false);
        submitMoveButton.setDisable(false);
        currentMoveLabel.setText(Integer.toString(0));
        currentTurnLabel.setText(Integer.toString(0));
        maxTurnsLabel.setText(Integer.toString(gameRoom.getMoves()));
    }

    /*
 returns a CellStatus enum according to the toggleGroup
  */
    public CellStatus getColorToggleGroupValue() {
        RadioButton chk = (RadioButton) colorToggleGroup.getSelectedToggle();
        String selectedRadioButton = chk.getText();
        CellStatus toggleGroupCellStatus;

        if (selectedRadioButton.equals("Black")) {
            toggleGroupCellStatus = CellStatus.BLACK;
        } else if (selectedRadioButton.equals("White")) {
            toggleGroupCellStatus = CellStatus.WHITE;
        } else {
            toggleGroupCellStatus = CellStatus.UNDEFINED;
        }

        return toggleGroupCellStatus;
    }

    public Button getEndTurnButton() {
        return endTurnButton;
    }

    public void setCurrentMoveLabelText(String labelText) {
        currentMoveLabel.setText(labelText);
    }

    public void setIsDisableSubmitMoveButton(boolean isDisabled) {
        submitMoveButton.setDisable(isDisabled);
    }

    public Button getSubmitMoveButton() {
        return submitMoveButton;
    }

    public void setCurrentTurnLabel(String s) {
        currentTurnLabel.setText(s);
    }

    public Button getUndoButton() {
        return undoMoveButton;
    }

    public void setIsDisableUndoMoveButton(boolean b) {
        undoMoveButton.setDisable(b);
    }

    public Button getEndTheGameButton() {
        return endTheGameButton;
    }

    public void setIsDisableRightVbox(boolean b) {
        rightPanelVBox.setDisable(b);
    }
}

