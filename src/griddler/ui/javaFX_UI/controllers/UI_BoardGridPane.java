package griddler.ui.javaFX_UI.controllers;

import griddler.logic.gameManager.gameRoom.GameRoom;
import griddler.logic.gameManager.gameRoom.board.Block;
import griddler.logic.gameManager.gameRoom.board.Cell;
import griddler.logic.gameManager.gameRoom.board.CellStatus;
import griddler.logic.gameManager.gameRoom.board.Slice;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.awt.*;
import java.util.HashSet;

public class UI_BoardGridPane extends GridPane {
    private GridPane gridPane;
    private HashSet<Point> selectedCells;
    private VBox[] colSlices;
    private HBox[] rowSlices;
    private GameRoom gameRoom;

    /*
    receives an init'ed game and defaults the gridPane
    */
    public void createGridPaneOfCells(GameRoom gameRoom) {
        initBoard(gameRoom);
        this.gameRoom = gameRoom;
        gridPane.setHgap(2);
        gridPane.setVgap(2);

        populateCells(gameRoom);
        populateRowSlices(gameRoom);
        populateColSlices(gameRoom);
    }

    private void initBoard(GameRoom gameRoom) {
        gridPane = new GridPane();
        selectedCells = new HashSet<>();
        colSlices = new VBox[(gameRoom.getBoardCols() + 1)];
        rowSlices = new HBox[(gameRoom.getBoardRows() + 1)];
    }

    private void populateCells(GameRoom gameRoom) {
        for (int i = 0; i < gameRoom.getBoardRows(); i++) {
            for (int j = 0; j < gameRoom.getBoardCols(); j++) {
                UI_BoardGUICell b = new UI_BoardGUICell(j, i);

                b.resize(5, 15);
                b.setOnMouseClicked(e -> {
                    if (selectedCells.contains(b.cell.CellToPoint())) {
                        selectedCells.remove(b.cell.CellToPoint());
                    } else {
                        selectedCells.add(b.cell.CellToPoint());
                    }
                    b.handleOnMouseClick_BoardCellUI();
                });
                gridPane.add(b, j, i);
            }
        }
    }

    private void populateColSlices(GameRoom gameRoom) {

        int colNum = 0;
        for (Slice slice : gameRoom.getColSlices().getSliceList()) {
            VBox vbox = new VBox();
            vbox.setSpacing(5);
            //TODO add styling

            for (Block block : slice.getBlocks()) {
                Label label = new Label("" + block.getBlock());


                vbox.getChildren().add(new Label("" + block.getBlock()));
            }
            colSlices[colNum] = vbox;
            gridPane.add(vbox, colNum, gameRoom.getBoardRows());
            colNum++;
        }
    }

    private void populateRowSlices(GameRoom gameRoom) {
        int rowNum = 0;
        for (Slice slice : gameRoom.getRowSlices().getSliceList()) {
            HBox hbox = new HBox();
            hbox.setSpacing(5);
            //TODO add styling

            for (Block block : slice.getBlocks()) {
                hbox.getChildren().add(new Label("" + block.getBlock()));
            }
            rowSlices[rowNum] = hbox;
            gridPane.add(hbox, gameRoom.getBoardCols(), rowNum);
            rowNum++;
        }
    }

    public GridPane getGridPane() {
        return gridPane;
    }

    public HashSet<Point> getSelectedCells() {
        return selectedCells;
    }

    public void clearSelectedCells() {
        selectedCells.clear();
    }

    public void updateBoardGUICells(Cell[][] cellBoardToUpdate) {
        int x, y;
        CellStatus currentCellStatus;
        UI_BoardGUICell currentNode;
        for (Node node : gridPane.getChildren()) {

            x = GridPane.getColumnIndex(node);
            y = GridPane.getRowIndex(node);

            if (node instanceof UI_BoardGUICell) {
                currentNode = (UI_BoardGUICell) node;
            } else {
                continue;
            }

            currentCellStatus = cellBoardToUpdate[y][x].getCellStatus();
            currentNode.resetSelect();
            currentNode.getStyleClass().clear();
            switch (currentCellStatus) {
                case BLACK:
                    currentNode.getStyleClass().add("BlackCellClass");
                    break;
                case WHITE:
                    currentNode.getStyleClass().add("WhiteCellClass");
                    break;
                case UNDEFINED:
                    currentNode.getStyleClass().add("UndefinedCellClass");
                    break;
            }

        }
    }

    public void updateBoardSliceStatus() {
        Node labelBlock;
        int index = 0;

        for (Slice slice : gameRoom.getRowSlices().getSliceList()) {
            for (Block block : slice.getBlocks()) {
                labelBlock = rowSlices[slice.getId()].getChildren().get(slice.getBlocks().indexOf(block));

                handleBlockPerfectCheck(labelBlock, block);
            }
            index = 0;
        }

        for (Slice slice : gameRoom.getColSlices().getSliceList()) {
            for (Block block : slice.getBlocks()) {
                labelBlock = colSlices[slice.getId()].getChildren().get(slice.getBlocks().indexOf(block));

                handleBlockPerfectCheck(labelBlock, block);
            }
            index = 0;
        }
    }

    private void handleBlockPerfectCheck(Node labelBlock, Block block) {
        Label label = (Label) labelBlock;
        if (block.isPerfect()) {
            label.getStyleClass().add("PerfectBlockClass");
        } else {
            label.getStyleClass().remove("PerfectBlockClass");
        }
    }
}
