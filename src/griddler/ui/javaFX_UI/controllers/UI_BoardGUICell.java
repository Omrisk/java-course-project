package griddler.ui.javaFX_UI.controllers;

import griddler.logic.gameManager.gameRoom.board.Cell;
import javafx.scene.control.Label;

/**
 * Created by Omri on 10/9/2016.
 */
public class UI_BoardGUICell extends Label {
    Cell cell;
    private boolean isSelected = false;

    public UI_BoardGUICell(int x, int y) {
        cell = new Cell(y, x);
        this.setMinSize(15, 15);
        this.setPrefSize(25, 25);
        this.getStyleClass().clear();
        this.getStyleClass().add("UndefinedCellClass");

        this.setOnMouseEntered(event -> {
            this.getStyleClass().add("HoveredCellClass");
        });

        this.setOnMouseExited(event -> {
            this.getStyleClass().remove("HoveredCellClass");
        });
    }

    public void handleOnMouseClick_BoardCellUI() {


        if (!isSelected) {
            this.getStyleClass().add("SelectedCellClass");
        } else {
            this.getStyleClass().remove("NotSelectedCellClass");

        }
        isSelected = !isSelected;
    }

    public Cell getCell() {
        return cell;
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }

    public void resetSelect() {
        isSelected = false;
    }
}
