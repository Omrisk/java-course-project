package griddler.ui.javaFX_UI.controllers;

import griddler.ui.javaFX_UI.mainScene.GriddlerGUIController;


interface CustomGriddlerController {
    void setControlsOnEvent(GriddlerGUIController griddlerGUIController);
}
