package griddler.ui.javaFX_UI.controllers;

import griddler.logic.gameManager.gameRoom.Player;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;

public class PlayerTableViewDataUnit {
    private final StringProperty playerName;
    private final StringProperty playerScore;
    private final StringProperty playerType;
    private final StringProperty playerID;


    public PlayerTableViewDataUnit(Player player) {
        this.playerName = new SimpleStringProperty(player.getName());
        this.playerScore = new SimpleStringProperty(String.valueOf(player.getStats().getScore()));
        this.playerType = new SimpleStringProperty(player.getPlayerType().toString());
        this.playerID = new SimpleStringProperty(player.getIDAsString());
    }

    public String getPlayerName() {
        return playerName.get();
    }

    public String getPlayerScore() {

        return playerScore.get();
    }

    public String getPlayerType() {
        return playerType.get();
    }

    public String getPlayerID() {
        return playerID.get();
    }

    public void setPlayerScore(Integer playerScore) {
        this.playerScore.setValue(String.valueOf(playerScore));
    }

    public StringProperty getPlayerNameProperty() {
        return playerName;
    }

    public StringProperty getPlayerTypeProperty() {
        return playerType;
    }

    public StringProperty getPlayerScoreProperty() {
        return playerScore;
    }

    public StringProperty getPlayerIDProperty() {
        return playerID;
    }
}
