package griddler.ui.javaFX_UI.mainScene;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/*
For creating observable instances of the game static variables
 */
public class GriddlerGUI_GameStaticParams {
    static final int SLEEP_TIME = 200;
    static private IntegerProperty currentMove = new SimpleIntegerProperty(0);
    static private IntegerProperty currentTurn = new SimpleIntegerProperty(0);
    private static int MAX_NUMBER_OF_TURNS = 0;
    private static boolean isGameInProgress = false;
    static final int MAX_NUMBER_OF_MOVES = 2;
    private static String cssResource = "/resources/css/Skin_A.css";

    public static String getCssResource() {
        return cssResource;
    }

    public static void setCssResource(String cssResource) {
        GriddlerGUI_GameStaticParams.cssResource = cssResource;
    }

    public static int getCurrentMove() {
        return currentMove.get();
    }

    public static IntegerProperty currentMoveProperty() {
        return currentMove;
    }

    public static void setCurrentMove(int currentMove) {
        GriddlerGUI_GameStaticParams.currentMove.set(currentMove);
    }

    public static int getCurrentTurn() {
        return currentTurn.get();
    }

    public static IntegerProperty currentTurnProperty() {
        return currentTurn;
    }

    public static void setCurrentTurn(int currentTurn) {
        GriddlerGUI_GameStaticParams.currentTurn.set(currentTurn);
    }

    public static void incCurrentMove() {
        int newValue = currentMove.get();
        newValue++;
        currentMove.set(newValue);
    }

    public static void incCurrentTurn() {
        int newValue = currentTurn.get();
        newValue++;
        currentTurn.set(newValue);
    }

    public static void decCurrentMove() {
        int newValue = currentMove.get();
        newValue--;
        currentMove.set(newValue);
    }

    /*
    we check if the game has ended, and adjust the static bool before returning
     */
    public static boolean isGameOverByTurn() {
        if (currentTurn.get() > MAX_NUMBER_OF_MOVES) {
            isGameInProgress = false;
        }
        return !isGameInProgress;
    }

    public static void setMaxTurns(int maxTurns) {
        GriddlerGUI_GameStaticParams.MAX_NUMBER_OF_TURNS = maxTurns;
        isGameInProgress = true;
    }
    public static boolean isGameInProgress() {
        return isGameInProgress;
    }

    public static void setIsGameInProgress(boolean b) {
        isGameInProgress = b;
    }
}
