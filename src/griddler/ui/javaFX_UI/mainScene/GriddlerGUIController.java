package griddler.ui.javaFX_UI.mainScene;

import griddler.logic.gameManager.GameManager;
import griddler.logic.gameManager.gameRoom.GameRoom;
import griddler.logic.gameManager.gameRoom.Player;
import griddler.logic.gameManager.gameRoom.Turn;
import griddler.logic.gameManager.logicEngine.LogicEngine;
import griddler.logic.gameManager.logicEngine.PerfectBlockFinderWithTask;
import griddler.ui.javaFX_UI.alertWindows.*;
import griddler.ui.javaFX_UI.controllers.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;

import java.io.File;

import static griddler.ui.javaFX_UI.mainScene.GriddlerGUI_GameStaticParams.MAX_NUMBER_OF_MOVES;

/**
 * Main Controller for the game, is the event handler for all of the games sub controllers
 * After events are processed by the sub controllers this class is  the single point of contact with the game logic
 * This ensures that the games are handled individually and we avoid future confilcts in turn handling
 */
public class GriddlerGUIController extends BorderPane implements EventHandler {
    private volatile GameRoom gameRoom;
    private BorderPane gameLayout;
    private UI_BoardGridPane ui_boardGridPane = new UI_BoardGridPane();
    private UI_BottomButtonsHBoxController ui_bottomButtonsHBoxController;
    private UI_RightVBoxController ui_rightVBoxController;
    private UI_LeftVBoxController ui_leftVBoxController;
    private Scene griddlerGameScene;
    private Task<Void> perfectBlockFinderTask;

    public GriddlerGUIController(BorderPane layout, Scene scene) {
        initGameLayout(layout);
        griddlerGameScene = scene;
    }

    private void initGameLayout(BorderPane layout) {
        gameLayout = layout;
        gameLayout.setBottom(ui_bottomButtonsHBoxController = new UI_BottomButtonsHBoxController());
        gameLayout.setRight(ui_rightVBoxController = new UI_RightVBoxController());
        gameLayout.setLeft(ui_leftVBoxController = new UI_LeftVBoxController());
        gameLayout.setCenter(new CustomTextMessagePane(GriddlerStaticText.WELCOME_TO_GRIDDLER));
        layout.getStyleClass().add("gameLayoutClass");
        setOnActionToControls();
    }

    private void setOnActionToControls() {
        ui_bottomButtonsHBoxController.setControlsOnEvent(this);
        ui_bottomButtonsHBoxController.getSkinSelectComboBox().valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                griddlerGameScene.getStylesheets().clear();
                if (newValue.equals("Skin A")) {
                    GriddlerGUI_GameStaticParams.setCssResource("/resources/css/Skin_A.css");
                } else if (newValue.equals("Skin B")) {
                    GriddlerGUI_GameStaticParams.setCssResource("/resources/css/Skin_B.css");
                }

                griddlerGameScene.getStylesheets().add(GriddlerGUI_GameStaticParams.getCssResource());
            }
        });
        ui_rightVBoxController.setControlsOnEvent(this);
        ui_leftVBoxController.setControlsOnEvent(this);
        ui_leftVBoxController.addListenerToPlayerTableView(ui_bottomButtonsHBoxController.getReplayMovesButton());
    }

    private void changeSkinHandler(String newValue) {

    }

    /*
    Handles actions performed on child controls
     */
    @Override
    public void handle(Event event) {
        Object source = event.getSource();

        if (source == ui_bottomButtonsHBoxController.getLoadGameButton()) {
            handleLoadGameButtonClick();
        } else if (source == ui_rightVBoxController.getSubmitMoveButton()) {
            handleSubmitMoveButtonClick();
        } else if (source == ui_rightVBoxController.getEndTurnButton()) {
            handleEndTurnButtonClick();
        } else if (source == ui_rightVBoxController.getUndoButton()) {
            handleUndoButtonClick();
        } else if (source == ui_rightVBoxController.getEndTheGameButton()) {
            handleEndTheGameButtonClick();
        } else if (source == ui_leftVBoxController.getPeekAtBoardButton()) {
            handlePeekAtBoardButtonClick();
        } else if (source == ui_leftVBoxController.getShowSelectedPlayerTurnListButton()) {
            handleShowSelectedPlayerTurnListClick();
        } else if (source == ui_bottomButtonsHBoxController.getReplayMovesButton()) {
            handleReplayMovesButtonClick();
        }
    }

    /**
     * Pops a replay turn window for the selected Player
     */
    private void handleReplayMovesButtonClick() {

        new ReplayMovesAlertWindow(gameRoom, getPlayerSelectedInTableView());
    }

    private void handleShowSelectedPlayerTurnListClick() {
        Player selectedPlayer = getPlayerSelectedInTableView();

        new ShowPlayerMovesWindow(selectedPlayer);
    }

    /*
    we received a valid request for a user "peek" and show an window with the AI board
     */
    private void handlePeekAtBoardButtonClick() {
        Player selectedPlayer = getPlayerSelectedInTableView();

        new PeekAtBoardWindow(gameRoom, selectedPlayer);
    }

    private Player getPlayerSelectedInTableView() {
        String peekablePlayer = ui_leftVBoxController.getSelectedPlayerIDInTableView();
        int idToFind = Integer.valueOf(peekablePlayer);
        return gameRoom.getPlayersList()
                .stream()
                .filter((e) -> e.getIdAsInt() == idToFind)
                .findFirst()
                .get();
    }

    /*
    If the user has chosen to end the game
     */
    private void handleEndTheGameButtonClick() {
        gameLayout.setCenter(new CustomTextMessagePane(GriddlerStaticText.USER_ENDED_GAME));
        GriddlerGUI_GameStaticParams.setIsGameInProgress(false);
        freezeGame();
    }

    /*
    We undo the last players move and adjust the move count and disable the undo button or enable the submit turn button if needed
     */
    private void handleUndoButtonClick() {
        gameRoom.getCurrentPlayer().performUndo();
        ui_boardGridPane.updateBoardGUICells(gameRoom.getCurrentPlayer().getPlayerBoard());
        ui_rightVBoxController.setIsDisableSubmitMoveButton(false);
        ui_boardGridPane.clearSelectedCells();
        //We check that we don't reach a negative turn
        if (GriddlerGUI_GameStaticParams.getCurrentMove() > 0) {
            GriddlerGUI_GameStaticParams.decCurrentMove();
            ui_rightVBoxController.setCurrentMoveLabelText(String.valueOf(GriddlerGUI_GameStaticParams.getCurrentMove()));
        }

        if (gameRoom.getCurrentPlayer().getTurnList().isEmpty()) {
            ui_rightVBoxController.setIsDisableUndoMoveButton(true);
        }
        updatePerfectBlocksOnBoard();
    }

    private void createAndSetUIBoardGridPane(GameRoom gameRoom) {
        ui_boardGridPane = new UI_BoardGridPane();
        ui_boardGridPane.createGridPaneOfCells(gameRoom);
        gameLayout.setCenter(new ScrollPane(ui_boardGridPane.getGridPane()));
    }


    /*
    we Iterate to the next player
    1.We reset the moves of the turn
    2. If human - we reload the board and start playing again
    3. If AI we handle a stream of the in the performAIPlayerTurns funct
     */
    private void handleEndTurnButtonClick() {
        GriddlerGUI_GameStaticParams.setCurrentMove(0);
        if (gameRoom.getCurrentPlayer().getStats().getScore() == 100) {
            gameWon();
        } else {
            gameRoom.advanceToNextPlayer();
            performAIPlayerTurns();
            //If we are now handling a human player
            if (gameRoom.getCurrentPlayer().getPlayerType() == Player.PlayerType.HUMAN) {
                updateBoardForHumanPlayer();
                checkForTurnEnd();
                ui_rightVBoxController.getEndTurnButton().setDisable(false);
            }
        }
        checkIfGameEnded();
    }

    /*
    The current player has won! We freeze the game and notify the user
     */
    private void gameWon() {
        freezeGame();
        GameWonAlert.showPlayerWonAlert(gameRoom.getCurrentPlayer());
    }

    /*
    if the game has ended by turns we "freeze" the board and notify the user
     */
    private void checkIfGameEnded() {
        if (GriddlerGUI_GameStaticParams.isGameOverByTurn()) {
            gameLayout.setCenter(new CustomTextMessagePane(GriddlerStaticText.TURNS_ENDED_GAME));
            freezeGame();
        }
    }

    private void freezeGame() {
        ui_rightVBoxController.setIsDisableRightVbox(true);
        ui_bottomButtonsHBoxController.setIsDisableLoadGame(false);
    }

    /*
    We handle a sequence of AI players if needed
     */
    private void performAIPlayerTurns() {
        int playersListLength = gameRoom.getPlayersList().size();

        while (gameRoom.getCurrentPlayer().getPlayerType() == Player.PlayerType.AI && !GriddlerGUI_GameStaticParams.isGameOverByTurn()) {
            performAITurn();
            gameRoom.advanceToNextPlayer();
        }
        /*
        We may have had aan AI player end the game by turns
         */
        checkIfGameEnded();
    }

    /*
    Update the board, buttons and labels
     */
    private void updateBoardForHumanPlayer() {
        ui_boardGridPane.updateBoardGUICells(gameRoom.getCurrentPlayer().getPlayerBoard());
        ui_rightVBoxController.setCurrentMoveLabelText(String.valueOf(GriddlerGUI_GameStaticParams.getCurrentMove()));
        ui_rightVBoxController.setIsDisableSubmitMoveButton(false);
        ui_leftVBoxController.updateCurrentPlayerStats(gameRoom.getCurrentPlayer());
    }

    private void handleSubmitMoveButtonClick() {
         /*
        Check if user has selected any cells
         */
        if (ui_boardGridPane.getSelectedCells().isEmpty()) {
            if (!EmptyMoveAlertWindow.userConfirmationNeeded()) {
                return;
            }
        }
        /*
        If we have a move to perform - collect the cells and update the board
         */
        updateGameBoard();
        ui_leftVBoxController.updateScoreBoard(gameRoom.getCurrentPlayer());
        ui_rightVBoxController.setIsDisableUndoMoveButton(false);

        if (GriddlerGUI_GameStaticParams.getCurrentMove() == MAX_NUMBER_OF_MOVES) {
            ui_rightVBoxController.setIsDisableSubmitMoveButton(true);
        }
    }

    /*
    Creates a turn from the cells selected on the UI_Board and submits to the game room
    We later adjust the text
     */
    private void updateGameBoard() {
        Turn turn = LogicEngine.createTurnFromSelectedCells(ui_boardGridPane.getSelectedCells(), ui_rightVBoxController.getColorToggleGroupValue());

        gameRoom.getPlayersList().get(0).performTurn(turn);
        ui_boardGridPane.updateBoardGUICells(gameRoom.getCurrentPlayer().getPlayerBoard());
        updatePerfectBlocksOnBoard();

        ui_boardGridPane.clearSelectedCells();
        GriddlerGUI_GameStaticParams.incCurrentMove();
        ui_rightVBoxController.setCurrentMoveLabelText(String.valueOf(GriddlerGUI_GameStaticParams.getCurrentMove()));
    }

    private void updatePerfectBlocksOnBoard() {
        perfectBlockFinderTask = new PerfectBlockFinderWithTask(gameRoom, GriddlerGUI_GameStaticParams.SLEEP_TIME);
        Thread perfectBlocksThread = new Thread(perfectBlockFinderTask);
        perfectBlocksThread.setDaemon(true);

        perfectBlockFinderTask.setOnSucceeded(event ->
                ui_boardGridPane.updateBoardSliceStatus()
        );

        perfectBlocksThread.start();
    }

    /*
    Runs a AI iteration of turns
     */
    private void performAITurn() {
        /*
        If we have reached the end of the list we adjust the currentTurn
         */

        checkForTurnEnd();

        int computerTurns = (int) (Math.random() % MAX_NUMBER_OF_MOVES + 1);
        for (int i = 0; i < computerTurns; i++) {
            Turn turn = LogicEngine.getAITurn(gameRoom.getBoardCols(), gameRoom.getBoardRows());
            gameRoom.getCurrentPlayer().performTurn(turn);
        }

    }

    /*
    1. We check if we have reached the end of the player list - if so inc the current turn
    2. we check if the game has ended after it's turn limit
    */
    private void checkForTurnEnd() {
        int playersListSize = gameRoom.getPlayersList().size();
        //1.
        if (gameRoom.getCurrentPlayer() == gameRoom.getPlayersList().get(playersListSize - 1)) {
            GriddlerGUI_GameStaticParams.incCurrentTurn();
            //2.
            if (!GriddlerGUI_GameStaticParams.isGameOverByTurn()) {
                ui_rightVBoxController.setCurrentTurnLabel(String.valueOf(GriddlerGUI_GameStaticParams.getCurrentTurn()));
            }
        }
    }

    private void handleLoadGameButtonClick() {
        //Loads the game and populates the scene if successful
        loadGame();
        createAndSetUIBoardGridPane(gameRoom);
        ui_bottomButtonsHBoxController.getLoadGameButton().setDisable(true);
        //Init the right VBox
        ui_rightVBoxController.initRightVBoxController(gameRoom);
        GriddlerGUI_GameStaticParams.setMaxTurns(gameRoom.getMoves());
        GriddlerGUI_GameStaticParams.setCurrentTurn(0);
        ui_leftVBoxController.initLeftVBoxController(gameRoom);
        /*
        In case the first player is AI we have him play
         */
        performAIPlayerTurns();

    }

    private void loadGame() {
        File selectedFile = chooseGameFile();
        GameManager gameManager = new GameManager();

        if (selectedFile != null) {
            try {
                gameManager.LoadNewGame(selectedFile.getPath());
                gameRoom = gameManager.getGameRoomList().get(0);
                if (playerConfirmsGame()) {
                    gameRoom.initGame();
                } else {
                    gameRoom = null;
                }
            } catch (Exception e) {
                //TODO progress bar
                System.out.print(e.getCause());
                GameExceptionCaught.GameExceptionAlertWindow(e.getMessage());
            }
        }

    }

    /*
    Shows the file chooser and returns the file path
     */
    private File chooseGameFile() {
        File file = null;
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choose Game XML FILE");
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XML Files", "*.xml"));
            file = fileChooser.showOpenDialog(null);
        } catch (Exception e) {
            System.out.print(e.getMessage());

        }

        return file;
    }

    private boolean playerConfirmsGame() {
        ConfirmGameStartAlertWindow confirmGameStartAlertWindow = new ConfirmGameStartAlertWindow();
        try {
            return confirmGameStartAlertWindow.showConfirmGameStartAlertWindow(gameRoom);
        } catch (Exception e) {
            GameExceptionCaught.GameExceptionAlertWindow(e.getMessage());
        }
        return false;
    }

    public static int getCurrentMove() {
        return GriddlerGUI_GameStaticParams.getCurrentMove();

    }
}