package griddler.ui.javaFX_UI.alertWindows;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

public class EmptyMoveAlertWindow {

    public static boolean userConfirmationNeeded() {
        boolean userDecision = false;
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Empty Move Submit");
        alert.setHeaderText("Are you sure you want to submit an empty move?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            userDecision = true;
        }

        return userDecision;
    }
}
