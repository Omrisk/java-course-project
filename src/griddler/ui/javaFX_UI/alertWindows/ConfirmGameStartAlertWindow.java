package griddler.ui.javaFX_UI.alertWindows;

import griddler.logic.gameManager.gameRoom.GameRoom;
import griddler.logic.gameManager.gameRoom.Player;
import griddler.ui.javaFX_UI.controllers.UI_BoardGridPane;
import griddler.ui.javaFX_UI.mainScene.GriddlerGUI_GameStaticParams;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class ConfirmGameStartAlertWindow extends Pane {
    @FXML
    private VBox gamePreviewBox;
    @FXML
    private AnchorPane boardPreviewPane;
    @FXML
    private VBox previewPlayerListVBox;
    @FXML
    private Button yesButton;
    @FXML
    private Button cancelButton;

    private UI_BoardGridPane previewGameBoard;
    private static boolean userDecision = false;
    Stage stage;

    public ConfirmGameStartAlertWindow() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("PreviewAndConfirmLoadGame.fxml"));
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        stage = new Stage();

        stage.setScene(new Scene(gamePreviewBox, Screen.getPrimary().getVisualBounds().getWidth() / 3, Screen.getPrimary().getVisualBounds().getHeight() / 3));
        stage.getScene().getStylesheets().clear();
        stage.getScene().getStylesheets().add(getClass().getResource(GriddlerGUI_GameStaticParams.getCssResource()).toExternalForm());
        stage.initModality(Modality.WINDOW_MODAL);
    }

    public boolean showConfirmGameStartAlertWindow(GameRoom gameRoom) throws IOException {
        initializeController(gameRoom);
        stage.showAndWait();
        return userDecision;
    }

    private void initializeController(GameRoom gameRoom) {
        yesButton.setOnMouseClicked((event) -> {
            userDecision = true;
            stage.close();
        });
        cancelButton.setOnMouseClicked((event -> {
            userDecision = true;
            stage.close();
        }));

        setBoardPreview(gameRoom);
        setPlayerNamePreview(gameRoom.getPlayersList());
    }

    private void setPlayerNamePreview(List<Player> playersList) {
        LinkedList<Label> playerNameList = new LinkedList<>();

        for (Player player : playersList) {
            String playerDetails = String.format("Name: %s, Type: %s", player.getName(), player.getPlayerType());
            playerNameList.add(new Label(playerDetails));
        }
        previewPlayerListVBox.getChildren().addAll(playerNameList);
    }

    private void setBoardPreview(GameRoom gameRoom) {
        previewGameBoard = new UI_BoardGridPane();
        previewGameBoard.createGridPaneOfCells(gameRoom);
        boardPreviewPane.getChildren().add(previewGameBoard.getGridPane());
    }
}
