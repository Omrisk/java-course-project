package griddler.ui.javaFX_UI.alertWindows;

import griddler.logic.gameManager.gameRoom.GameRoom;
import griddler.logic.gameManager.gameRoom.Player;
import griddler.logic.gameManager.gameRoom.Turn;
import griddler.ui.javaFX_UI.controllers.UI_BoardGridPane;
import griddler.ui.javaFX_UI.mainScene.GriddlerGUI_GameStaticParams;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;

public class ReplayMovesAlertWindow extends Pane {

    @FXML
    private VBox gamePreviewBox;
    @FXML
    private Label nameLabel;
    @FXML
    private AnchorPane boardPreviewPane;
    @FXML
    private Button prevButton;
    @FXML
    private Button nextButton;
    @FXML
    private Button closeButton;

    Stage stage;
    GameRoom gameRoom;
    UI_BoardGridPane replayBoardGridPane;
    Player selectedPlayer;
    Turn turnToReturnTo;

    public ReplayMovesAlertWindow(GameRoom gameRoom, Player selectedPlayerIDInTableView) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ReplayMovesWindow.fxml"));
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        stage = new Stage();
        stage.setScene(new Scene(gamePreviewBox, Screen.getPrimary().getVisualBounds().getWidth() / 3, Screen.getPrimary().getVisualBounds().getHeight() / 3));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.getScene().getStylesheets().clear();
        stage.getScene().getStylesheets().add(getClass().getResource(GriddlerGUI_GameStaticParams.getCssResource()).toExternalForm());
        initializeController(gameRoom, selectedPlayerIDInTableView);
        stage.showAndWait();
    }

    private void initializeController(GameRoom gameRoom, Player selectedPlayerIDInTableView) {
        this.gameRoom = gameRoom;
        this.selectedPlayer = selectedPlayerIDInTableView;
        turnToReturnTo = selectedPlayer.getTurnList().getLast();
        stage.setOnHiding(new EventHandler<WindowEvent>() {

            @Override
            public void handle(WindowEvent event) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        restorePlayerTurns();
                    }
                });
            }
        });

        if (selectedPlayer.isTurnListEmpty()) {
            prevButton.setDisable(true);
        }
        if (selectedPlayer.isRedoListEmpty()) {
            nextButton.setDisable(true);
        }
        prevButton.setOnMouseClicked((event) -> {
            handleUndo();
        });
        nextButton.setOnMouseClicked((event -> {
            handleRedo();
        }));
        closeButton.setOnMouseClicked((event) -> {
        });
        closeButton.setOnMouseClicked((event -> {
            stage.close();
        }));

        initializeBoard();
    }

    private void handleRedo() {
        selectedPlayer.performRedo();
        replayBoardGridPane.updateBoardGUICells(selectedPlayer.getPlayerBoard());

        if (selectedPlayer.isRedoListEmpty()) {
            nextButton.setDisable(true);
        }
        if (!selectedPlayer.isTurnListEmpty()) {
            prevButton.setDisable(false);
        }
    }

    private void handleUndo() {
        selectedPlayer.performUndo();
        replayBoardGridPane.updateBoardGUICells(selectedPlayer.getPlayerBoard());

        if (selectedPlayer.isTurnListEmpty()) {
            prevButton.setDisable(true);
        }
        if (!selectedPlayer.isRedoListEmpty()) {
            nextButton.setDisable(false);
        }
    }

    private void initializeBoard() {
        replayBoardGridPane = new UI_BoardGridPane();
        replayBoardGridPane.createGridPaneOfCells(gameRoom);
        replayBoardGridPane.updateBoardGUICells(selectedPlayer.getPlayerBoard());
        boardPreviewPane.getChildren().add(replayBoardGridPane.getGridPane());
        boardPreviewPane.setDisable(true);
        boardPreviewPane.setOpacity(30);
        nameLabel.setText(selectedPlayer.getName());
    }

    /**
     * We restore the player to the way he was
     */
    private void restorePlayerTurns() {
        Turn turn = selectedPlayer.getTurnList().getLast();

        if (selectedPlayer.getTurnList().contains(turnToReturnTo)) {
            while (turn != turnToReturnTo) {
                selectedPlayer.performUndo();
                turn = selectedPlayer.getTurnList().getLast();
            }
        } else {
            while (turn != turnToReturnTo) {
                selectedPlayer.performRedo();
                turn = selectedPlayer.getRedoList().getFirst();
            }
        }
    }

}
