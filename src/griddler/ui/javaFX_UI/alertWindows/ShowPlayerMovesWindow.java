package griddler.ui.javaFX_UI.alertWindows;

import griddler.logic.gameManager.gameRoom.Player;
import griddler.logic.gameManager.logicEngine.LogicEngine;
import griddler.ui.javaFX_UI.mainScene.GriddlerGUI_GameStaticParams;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.stage.Screen;
import javafx.stage.Stage;

/*
We Create a window with a text area to see the selcted players moves to this point
 */
public class ShowPlayerMovesWindow {
    Stage stage;
    Label header;
    Button closeButton;
    BorderPane movesBoard;

    TextArea turnList;

    public ShowPlayerMovesWindow(Player selectedPlayer) {
        initializeComponents(selectedPlayer);
        stage.setScene(new Scene(new ScrollPane(movesBoard), Screen.getPrimary().getVisualBounds().getWidth() / 3, Screen.getPrimary().getVisualBounds().getHeight() / 3));
        stage.getScene().getStylesheets().clear();
        stage.getScene().getStylesheets().add(getClass().getResource(GriddlerGUI_GameStaticParams.getCssResource()).toExternalForm());
        stage.showAndWait();
    }

    private void initializeComponents(Player selectedPlayer) {
        stage = new Stage();
        intHeader(selectedPlayer.getName());
        initTextArea(selectedPlayer);
        initCloseButton();
        setBorderPaneLayout();
    }

    private void intHeader(String name) {
        header = new Label(name + "'s Moves!");
        header.setStyle("-fx-background-color: antiquewhite");
    }

    private void initTextArea(Player selectedPlayer) {
        String playerTurns = LogicEngine.getTurnListAsText(selectedPlayer.getTurnList());
        turnList = new TextArea(playerTurns);
    }

    private void setBorderPaneLayout() {
        movesBoard = new BorderPane();
        movesBoard.setTop(header);
        movesBoard.setCenter(turnList);
        movesBoard.setBottom(closeButton);
    }

    private void initCloseButton() {
        //Init button
        closeButton = new Button("Close");
        closeButton.setOnMouseClicked(e -> stage.close());
    }
}
