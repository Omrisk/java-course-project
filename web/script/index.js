/**
 * Created by Saar on 04/10/2016.
 */
function checkIfWritten() {
    var userName = document.getElementById('username').value;
    if (userName == null || userName == "") {
        document.getElementById('submitButton').disabled = true;
    } else {
        document.getElementById('submitButton').disabled = false;
    }
}